# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Derived from https://www.tensorflow.org/tutorials/structured_data/time_series
# Copyright 2023 Ralf Lübben, original Copyright 2019 The TensorFlow Authors.


import numpy as np
from matplotlib import rcParams
rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 5
rcParams['font.size'] = 18
import matplotlib.pyplot as plt
from copy import deepcopy
import gc
import matplotlib.patches as patches

class WindowGenerator():
  def __init__(self, input_start, input_width, label_width, shift,
               train_df, val_df, test_df,
               label_columns=None, feature_columns=None, future_iats=False):

    print("TAGS " + str(train_df.attrs.keys()))
    if "tag" in train_df.attrs.keys():
      self.train_df_slice_tags = train_df.attrs["tag"]
      self.val_df_slice_tags = val_df.attrs["tag"]
      self.test_df_slice_tags = test_df.attrs["tag"]
      print(test_df.attrs["tag"].shape)
    else:
      self.train_df_slice_tags = None
      self.val_df_slice_tags = None
      self.test_df_slice_tags = None


    self.train_mean = train_df.mean(axis=(0,1))
    self.train_std = train_df.std(axis=(0,1))
    self.future_iats = future_iats

    # Store the raw data.
    self.train_df = (train_df - self.train_mean) / self.train_std
    del train_df
    self.val_df = (val_df - self.train_mean) / self.train_std
    del val_df
    self.test_df = (test_df - self.train_mean) / self.train_std
    del test_df

    self.train_df_slice = None
    self.val_df_slice = None
    self.test_df_slice = None

    
    # Work out the label column indices.
    self.label_columns = label_columns
    if label_columns is not None:
      self.label_columns_indices = {name: i for i, name in enumerate(label_columns)}
    self.column_indices = {name: i for i, name in enumerate(self.train_df.coords["values"].values)}

    # set the normalize value used for labels
    self.label_norm = self.train_std
    if self.label_columns is not None:
      self.label_norm = np.stack([self.label_norm[self.column_indices[name]] for name in self.label_columns], axis=-1)

    self.label_mean = self.train_mean
    if self.label_columns is not None:
      self.label_mean = np.stack([self.label_mean[self.column_indices[name]] for name in self.label_columns], axis=-1)

    # Work out the label column indices.
    self.feature_columns = feature_columns
    if feature_columns is not None:
      self.feature_columns_indices = {name: i for i, name in enumerate(feature_columns)}
    self.feature_indices = {name: i for i, name in enumerate(self.train_df.coords["values"].values)}

    # Work out the window parameters.
    self.input_start = input_start
    self.input_width = input_width
    self.label_width = label_width
    self.shift = shift

    self.total_window_size = input_start + input_width + shift + label_width

    self.input_slice = slice(input_start, input_start+input_width)
    self.input_indices = np.arange(self.total_window_size)[self.input_slice]

    self.label_start = input_start + input_width + shift
    self.labels_slice = slice(self.label_start, self.label_start + self.label_width)
    self.label_indices = np.arange(self.total_window_size)[self.labels_slice]

    # only relevant if we know future interarrivals
    self.input_slice_future = slice(input_start, self.label_start + self.label_width)
    self.input_future_indices = np.arange(self.total_window_size)[self.input_slice_future]

    # create slices, this causes deletion of complete time series and only keeps slices
    x,y = self.test
    x,y = self.val
    x,y = self.train

  def __repr__(self):
    return '\n'.join([
        f'Input indices: {self.input_indices}',
        f'Label indices: {self.label_indices}',
        f'Label column name(s): {self.label_columns}'])

  def get_label_norm(self):
    return self.label_norm[0]

  def make_dataset(self, data):
    
    labels = deepcopy(data.values[:, self.labels_slice,:])
    
    if self.label_columns is not None:
      labels = np.stack([labels[:, :, self.column_indices[name]] for name in self.label_columns], axis=-1)
    labels=np.reshape(labels, (-1,labels.shape[1]))

    if self.future_iats == False:
      inputs = deepcopy(data.values[:, self.input_slice, :])
      if self.feature_columns is not None:
        inputs = np.stack([inputs[:, :, self.feature_indices[name]] for name in self.feature_columns], axis=-1)
    else: # also use future values
      inputs = deepcopy(data.values[:, self.input_slice_future, :])
      if self.feature_columns is not None:
        # np.concatenate((a, b), axis = 0)
        tmp = None
        for name in self.feature_columns:
            data = inputs[:, :, self.feature_indices[name]]
            if name in self.label_columns:
              i1=self.input_width
              i2=i1+self.label_width
              data[:,i1:i2] = 0
            if tmp is None:
              tmp = data
            else:
              tmp = np.stack([tmp, data], axis=-1)
        inputs = tmp
    return  inputs, labels

  @property
  def train(self):
    if self.train_df_slice is None:
      self.train_df_slice = self.make_dataset(self.train_df)
      del self.train_df
      self.train_df = None
      gc.collect()
    return self.train_df_slice

  @property
  def val(self):
    if self.val_df_slice is None:
      self.val_df_slice = self.make_dataset(self.val_df)
      del self.val_df
      self.val_df = None
      gc.collect()
    return self.val_df_slice

  @property
  def test(self):
    if self.test_df_slice is None:
      self.test_df_slice = self.make_dataset(self.test_df)
      del self.test_df
      self.test_df = None
      gc.collect()
    return self.test_df_slice

  def example(self):
    """Get and cache an example batch of `inputs, labels` for plotting."""
    result = getattr(self, '_example', None)
    if result is None:
      # No example batch was found, so get one from the `.train` dataset
      result = next(iter(self.val))
      # And cache it for next time
      self._example = result
    return result

  def plot(self, model=None, plot_col='delay', max_subplots=10, run_dir=".", fig_extension=""):
    inputs, labels = self.test
    
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[plot_col]
    max_n = min(max_subplots, len(inputs))
    plt.figure(figsize=(12, int(3*max_n)))
    for n in range(max_n):
      plt.subplot(max_n, 1, n+1)
      plt.ylabel(f'{plot_col} [s]')
 
      x = (inputs[n, :, feature_col_index] * self.train_std[plot_col_index].values) + self.train_mean[plot_col_index].values
      plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs', marker='.', zorder=-10)

      if self.label_columns:
        label_col_index = self.label_columns_indices.get(plot_col, None)
      else:
        label_col_index = plot_col_index

      if label_col_index is None:
        continue
     
      l = "label" # add measurement tag if defined, e.g. mm1_75
      if self.test_df_slice_tags is not None:
          print(self.test_df_slice_tags[n])
          l = l + " " + str(self.test_df_slice_tags[n][0])
      else:
          print("No tags")

      y = (labels[n, :]*self.get_label_norm())+self.train_mean[plot_col_index].values
      plt.scatter(self.label_indices, y,
                  edgecolors='k', label=l, c='#2ca02c', s=32)
      if model is not None:
        predictions = (model(inputs[0:max_subplots,:,:])*self.get_label_norm())+self.train_mean[plot_col_index].values
        plt.scatter(self.label_indices, predictions[n, :],
                    marker='X', edgecolors='k', label='predicted quantile',
                    c='#ff7f0e', s=32)
      #if n == 0:
      plt.legend()

    plt.xlabel('packet [#]')
    plt.tight_layout()
    if fig_extension != "":
      tag = 'distance_' + fig_extension + ".pdf"
    else:
      tag='distance.pdf'

    fn=run_dir + '/' + tag
    print ("Save: " + fn)
    plt.savefig(fn)

  def plot_single(self, model=None, plot_col='delay', max_subplots=25, run_dir=".", fig_extension=""):
    inputs, labels = self.test
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[plot_col]
    max_n = min(max_subplots, len(inputs))
    for n in range(max_n):
      plt.figure(figsize=[2*6.4, 4.8])
      plt.ylabel(f'{plot_col} [s]')
 
      x = (inputs[n, :, feature_col_index] * self.train_std[plot_col_index].values) + self.train_mean[plot_col_index].values
      plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs', linestyle='None', marker='D', zorder=-10)

      if self.label_columns:
        label_col_index = self.label_columns_indices.get(plot_col, None)
      else:
        label_col_index = plot_col_index

      if label_col_index is None:
        continue
     
      l = "label" # add measurement tag if defined, e.g. mm1_75
      if self.test_df_slice_tags is not None:
          print(self.test_df_slice_tags[n])
          l = l + " " + str(self.test_df_slice_tags[n][0])
      else:
          print("No tags")

      y = (labels[n, :]*self.get_label_norm())+self.train_mean[plot_col_index].values
      plt.scatter(self.label_indices, y,
                  edgecolors='k', label=l, c='#2ca02c', s=32)
      if model is not None:
        predictions = (model(inputs[0:max_subplots,:,:])*self.get_label_norm())+self.train_mean[plot_col_index].values
        plt.scatter(self.label_indices, predictions[n, :],
                    marker='X', edgecolors='k', label='predicted quantile',
                    c='#ff7f0e', s=32)
      #if n == 0:
      plt.legend(loc="lower right")
      plt.xlabel('packet [#]')
      plt.tight_layout()
      if fig_extension != "":
        tag = 'distance_' + fig_extension + "_" + str(n) + ".pdf"
      else:
        tag='distance.pdf'

      fn=run_dir + '/' + tag
      print ("Save: " + fn)
      plt.savefig(fn)
      plt.savefig(fn+".png")

  def plot_mean(self, model=None, plot_col='delay', run_dir=".", fig_extension=""):
    inputs, labels = self.test
    
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[plot_col]
    plt.figure(figsize=[2*6.4, 4.8])
   
    plt.ylabel(f'{plot_col} [s]')

    x = np.quantile(inputs[:, :, feature_col_index], 0.95, axis=0) * self.train_std[plot_col_index].values + self.train_mean[plot_col_index].values
    print(x.shape)
    plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs', linestyle='None', marker='D', markersize=10, zorder=-10)

    if self.label_columns:
      label_col_index = self.label_columns_indices.get(plot_col, None)
    else:
      label_col_index = plot_col_index

    y = np.quantile(labels[:, :],0.95, axis=0)*self.get_label_norm()+self.train_mean[plot_col_index].values
    plt.scatter(self.label_indices, y,
                edgecolors='k', label='empirical quantile', c='#2ca02c', s=32)
    if model is not None:
      predictions = np.mean(model.predict(inputs),axis=0)*self.get_label_norm()+self.train_mean[plot_col_index].values
      plt.scatter(self.label_indices, predictions,
                  marker='X', edgecolors='k', label='mean predicted quantile',
                  c='#ff7f0e', s=32)
    
    plt.legend()

    plt.xlabel('packet [#]')
    plt.tight_layout()
    if fig_extension != "":
      tag = 'mean_distance_' + fig_extension + ".pdf"
    else:
      tag='mean_distance.pdf'
    fn=run_dir + '/' + tag
    print ("Save: " + fn)
    plt.savefig(fn)

  def plot_ts_delay(self, plot_col='delay', run_dir=".", fig_extension=""):
    inputs, labels = self.test
    
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[plot_col]
    plt.figure(figsize=(12, 3))
    plt.ylabel(f'{plot_col} [s]')
    
    x = (inputs[0, :, feature_col_index] * self.train_std[plot_col_index].values) + self.train_mean[plot_col_index].values
    plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs', marker='.', zorder=-10)

    y = (labels[0, :]*self.get_label_norm())+self.train_mean[plot_col_index].values
    plt.scatter(self.label_indices, y,edgecolors='k', label='features', c='#2ca02c', s=32)
    ax = plt.gca()
    ylim_1,ylim=ax.get_ylim()
    ax.set_ylim([ylim_1, 1.3*ylim])

    dx=(self.input_indices[-1]-self.input_indices[0])
    plt.arrow(self.input_indices[0],1.1*ylim,dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
    plt.arrow(self.input_indices[-1],1.1*ylim,-dx,0, width=0.01, head_width=0.1, head_length=2,length_includes_head=True)

    x_text=self.input_indices[0]+(self.input_indices[-1]-self.input_indices[0])/2
    ax.annotate("inputs 1",
            xy=(self.input_indices[0], 1.15*ylim), xycoords='data',
            xytext=(x_text, 1.15*ylim), textcoords='data',
            horizontalalignment="center"
    )

    dx=(self.label_indices[-1]-self.label_indices[0])
    plt.arrow(self.label_indices[0],1.1*ylim,dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
    plt.arrow(self.label_indices[-1],1.1*ylim,-dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
            
    x_text=self.label_indices[0]+(self.label_indices[-1]-self.label_indices[0])/2
    ax.annotate("labels",
            xy=(self.label_indices[0], 1.15*ylim), xycoords='data',
            xytext=(x_text, 1.15*ylim), textcoords='data',
            horizontalalignment="center"
    )
     
    if self.label_columns:
      label_col_index = self.label_columns_indices.get(plot_col, None)
    else:
      label_col_index = plot_col_index
        
    plt.legend()

    plt.xlabel('packet [#]')
    plt.tight_layout()
    tag = 'delay_ts_' + fig_extension + ".pdf"
    fn=run_dir + '/' + tag
    print ("Save: " + fn)
    plt.savefig(fn)

  def plot_iat_delay(self, plot_col='iat', run_dir=".", fig_extension=""):
    inputs, labels = self.test
    
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[plot_col]
    plt.figure(figsize=(12, 3))
    plt.ylabel(f'{plot_col} [s]')
    
    # first iats
    x = (inputs[0, :, feature_col_index] * self.train_std[plot_col_index].values) + self.train_mean[plot_col_index].values
    plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs 2', marker='.', zorder=-10)

    # future iats
    x = (inputs[0, :, feature_col_index] * self.train_std[plot_col_index].values) + self.train_mean[plot_col_index].values
    plt.plot(self.input_future_indices, x[0:len(self.input_future_indices)], label='inputs 3', marker='.', zorder=-10)
    
    ax = plt.gca()
    ylim_1,ylim=ax.get_ylim()
    ax.set_ylim([ylim_1, 1.3*ylim])

    dx=(self.input_indices[-1]-self.input_indices[0])
    plt.arrow(self.input_indices[0],1.1*ylim,dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
    plt.arrow(self.input_indices[-1],1.1*ylim,-dx,0, width=0.01, head_width=0.1, head_length=2,length_includes_head=True)

    x_text=self.input_indices[0]+(self.input_indices[-1]-self.input_indices[0])/2
    ax.annotate("inputs 2",
            xy=(self.input_indices[0], 1.15*ylim), xycoords='data',
            xytext=(x_text, 1.15*ylim), textcoords='data',
            horizontalalignment="center"
    )

    dx=(self.label_indices[-1]-self.label_indices[0])
    plt.arrow(self.label_indices[0],1.1*ylim,dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
    plt.arrow(self.label_indices[-1],1.1*ylim,-dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
            
    x_text=self.label_indices[0]+(self.label_indices[-1]-self.label_indices[0])/2
    ax.annotate("inputs 3",
            xy=(self.label_indices[0], 1.15*ylim), xycoords='data',
            xytext=(x_text, 1.15*ylim), textcoords='data',
            horizontalalignment="center"
    )

    if self.label_columns:
      label_col_index = self.label_columns_indices.get(plot_col, None)
    else:
      label_col_index = plot_col_index
        
    plt.legend()

    plt.xlabel('packet [#]')
    plt.tight_layout()
    tag = 'iat_ts_' + fig_extension + ".pdf"
    fn=run_dir + '/' + tag
    print ("Save: " + fn)
    plt.savefig(fn)

  def stats_per_tag(self, model, plot_col='delay', fig_extension=""):
    inputs, labels = self.test
    plot_col_index = self.column_indices[plot_col]
    labels = labels*self.get_label_norm()+self.train_mean[plot_col_index].values
    predictions = (model.predict(inputs)*self.get_label_norm())+self.train_mean[plot_col_index].values
    
    utags = []
    for t in self.test_df_slice_tags:
      if t not in utags:
        utags.append(t[0])
    d={}
    q={}
    dc={}
    qc={}
    lt={}
    pt={}
    dt={}
    for t in utags:
      d[t]=0
      q[t]=0
      dc[t]=0
      qc[t]=0
      lt[t]=[]
      pt[t]=[]
      dt[t]=[]
    n=0
    label_idx=-1
    for tag in self.test_df_slice_tags:
      #print(tag)
      d[tag[0]]+=(np.sum(predictions[n, label_idx] -  labels[n, label_idx]))
      dc[tag[0]]+=1#predictions.shape[1]
      q[tag[0]]+=(np.sum(predictions[n, label_idx] <  labels[n, label_idx]))
      qc[tag[0]]+=1#predictions.shape[1]
      #print(np.sum(np.abs(predictions[n, label_idx] -  labels[n, label_idx])))
      lt[tag[0]].append(labels[n, label_idx])
      pt[tag[0]].append(predictions[n, label_idx])
      if labels[n, label_idx] > predictions[n, label_idx]:
          dt[tag[0]].append(predictions[n, label_idx]-labels[n, label_idx])
      n=n+1
    
    plt_data = []
    msum=0
    sumc=0
    print("Stat: epsilon mean_distance emp_quantile mean_q_pred") 
    for i in q.keys():
      print("Stat:", i, round(q[i]/qc[i],3), round(d[i]/dc[i],3), round(np.quantile(lt[i], 0.95),3), round(np.mean(pt[i]),3))
      msum+=q[i]
      sumc+=qc[i]
      plt_data.append(dt[i])
        
    fig1, ax1 = plt.subplots()
    ax1.boxplot(plt_data)
    plt.tight_layout()
    tag = 'tag_stats_' + fig_extension + ".pdf"
    fn='./' + tag
    print ("Save: " + fn)
    plt.savefig(fn)
    plt.close()
    
    fig1, ax1 = plt.subplots()
    for i in q.keys():
      plt.hist(dt[i], 50, density=True)
    plt.tight_layout()
    tag = 'hist_' + fig_extension + ".pdf"
    fn='./' + tag
    print ("Save: " + fn)
    plt.savefig(fn)
     

    #print(msum/sumc)
    #import xarray as xr
    #df=xr.DataArray([labels, predictions], dims=["true","pred", "values"])
    #df.attrs["tag"]=self.test_df_slice_tags

  # def stats(self, model, plot_col='delay'):
  #   inputs, labels = self.test
  #   plot_col_index = self.column_indices[plot_col]
  #   labels = labels*self.get_label_norm()+self.train_mean[plot_col_index].values
  #   predictions = (model.predict(inputs)*self.get_label_norm())+self.train_mean[plot_col_index].values

  #   d=0.0
  #   q=0.0
  #   dc=0.0
  #   qc=0.0
  #   n=0

  #   for n in range(0,predictions.shape[0]):
  #     d+=(np.sum(predictions[n, :] -  labels[n, :]))
  #     dc+=predictions.shape[1]
  #     q+=(np.sum(predictions[n, :] <  labels[n, :]))
  #     qc+=predictions.shape[1]

  #   print(q/qc)
  #   print(d/dc)

    