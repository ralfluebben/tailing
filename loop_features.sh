# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

learning_rate=( 0.0 1e-3 1e-4 1e-5 )
input_type=( delay_only ) # delay_only delay_iat_short delay_iat_long
input_width=( 100 1 ) # 400 100 1
input_ref=400
data_tag=( mm1_75 mw1_75 ww1_75 on_off_50_bf on_off_100_bf periodic_on_off ) #( mm1_75 mw1_75 ww1_75 on_off on_off_100 periodic_on_off )
architecture=( Dense LSTM ) # Dense LSTM

for n in "${input_type[@]}"
do
~/bin/parallel -j 8 --delay 60 python3 window.py -t {3} -s '$(expr 400 \- {4} )' -w {4} -l 200 -r {1} -d {2} -f ${n} -u Hyperband -j {#} ::: ${learning_rate[@]} ::: ${data_tag[@]} ::: ${architecture[@]} ::: ${input_width[@]}
done

