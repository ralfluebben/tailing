# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

learning_rate=( 0.0 )
input_type=( delay_only ) # delay_only delay_iat_long
input_width=( 1 100 400 )
input_ref=400
data_tag=( mm1_75 mw1_75 ww1_75 on_off on_off_100 mlab on_off_50_bf on_off_100_bf periodic_on_off mlab ) # mix1 mix3 periodic_on_off
architecture=( LSTM Dense )

for n in "${input_width[@]}"
do
for a in "${architecture[@]}"
do
for t in "${data_tag[@]}"
do
for i in "${input_type[@]}"
do
echo "${a}${t}_${i}_${n}_"
tid=`python3 get_best_tuner_model.py results/${a}${t}_${i}_${n}_ | head -n 2 | tail -n 1`
echo $tid
input_start=$(($input_ref-$n))
python3 window.py -t ${a} -s $input_start -w $n -l 200 -r 0.0 -d $t -f $i -u Hyperband -j 10 -m $tid 2>/dev/null | grep "Stat\|Result"
done
done
done
done
