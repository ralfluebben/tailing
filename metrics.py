# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import tensorflow as tf
from tensorflow import keras
class QuantileDistance(tf.keras.metrics.Metric):
    def __init__(self, name='quantile_distance', **kwargs):
        self.q = 1-kwargs["quantile"]
        self.normalize = kwargs["normalize"]
        # remove keys, else error in super()
        del kwargs["quantile"]
        del kwargs["normalize"]
        super(QuantileDistance, self).__init__(name=name, **kwargs)
        self.quantile_distance = self.add_weight(name='quantile_distance', initializer='zeros')
        self.quantile_distance_count = self.add_weight(name='quantile_distance_count', initializer='zeros')
        #self.quantile_distance_count_greater = self.add_weight(name='quantile_distance_count_greater', initializer='zeros')
        
    def update_state(self, y_true, y_pred, sample_weight=None):
        diff = y_true - tf.squeeze(y_pred)
        quantileCondition = tf.math.greater(diff, 0)
        qc=tf.cast(quantileCondition,tf.float32)
        self.quantile_distance.assign_add(tf.math.reduce_sum(tf.abs(diff)))
        self.quantile_distance_count.assign_add(tf.cast(tf.size(diff),tf.float32))
        #self.quantile_distance_count_greater.assign_add(tf.math.reduce_sum(qc))

    def result(self):
        w = 1.0
        #qc = self.quantile_distance_count_greater/self.quantile_distance_count
        #if qc > self.q:
        #    w += (qc-self.q)/self.q
        return self.quantile_distance/self.quantile_distance_count * self.normalize #* w

    def reset_state(self):
        # The state of the metric will be reset at the start of each epoch.
        self.quantile_distance.assign(0.)
        self.quantile_distance_count.assign(0.)
        self.quantile_distance_count_greater.assign(0.)

class QuantileMetric(tf.keras.metrics.Metric):
    def __init__(self, name='quantile_metric', **kwargs):
        super(QuantileMetric, self).__init__(name=name, **kwargs)
        self.quantile_metric = self.add_weight(name='quantile_metric', initializer='zeros')
        self.quantile_metric_count = self.add_weight(name='quantile_metric_count', initializer='zeros')

    def update_state(self, y_true, y_pred, sample_weight=None):
        quantileCondition = tf.math.greater(y_true, tf.squeeze(y_pred))
        qc=tf.math.reduce_sum(tf.cast(quantileCondition,tf.float32))
        self.quantile_metric.assign_add(qc)
        self.quantile_metric_count.assign_add(tf.cast(tf.size(quantileCondition),tf.float32))
   
    def result(self):
        return self.quantile_metric/self.quantile_metric_count

    def reset_state(self):
        self.quantile_metric.assign(0.)
        self.quantile_metric_count.assign(0)

# class QuantileAccuracy(tf.keras.metrics.Metric):
#     def __init__(self, name='quantile_accuracy', **kwargs):
#         super(QuantileAccuracy, self).__init__(name=name, **kwargs)
#         self.quantile_accuracy = self.add_weight(name='quantile_accuracy', initializer='zeros')
#         self.quantile_accuracy_count = self.add_weight(name='quantile_accuracy_count', initializer='zeros')

#     def update_state(self, y_true, y_pred, sample_weight=None):
#         quantileCondition = y_true > y_pred
#         self.quantile_accuracy.assign_add(tf.math.reduce_sum(tf.cast(quantileCondition,tf.float32)))
#         self.quantile_accuracy_count.assign_add(y_true.shape[0]*y_true.shape[1])

#     def result(self):
#         return tf.math.abs(1.0-self.quantile_accuracy/self.quantile_accuracy_count-q_alpha)

#     def reset_states(self):
#         # The state of the metric will be reset at the start of each epoch.
#         self.quantile_accuracy.assign(0.)
#         self.quantile_accuracy_count.assign(0.)

class TerminateImmediatelyCallback(keras.callbacks.Callback):
    """Stop training when the loss is at its min, i.e. the loss stops decreasing.
    """
    def __init__(self):
        super(TerminateImmediatelyCallback, self).__init__()
    def on_train_begin(self, logs=None):
        print("TERMINATE EARLY")
        self.model.stop_training = True