# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import os
import os.path
import numpy as np
import json
import sys
filter_tag = sys.argv[1]
data_aggregated = None
results = []
for dirpath, dirnames, filenames in os.walk("."):
    for filename in [f for f in filenames if f.endswith("trial.json")]:
        fn = os.path.join(dirpath, filename)
        if filter_tag in fn:
            with open(fn) as json_file:
                data = json.load(json_file)
                #print(data)
                if (data['status'] == 'COMPLETED'):
                    try:
                        #if (data['metrics']['metrics']['quantile_metric']['observations'][0]['value'][0] <= 0.05):
                        data['path']=fn
                        results.append(data)
                    except:
                        print("Missing metric")
                        print(data['status'])

sorted_results=sorted(results, key=lambda k: k['score'], reverse=False)
print("############################")
#for r in sorted_results:
#    print(r['metrics']['metrics']['quantile_distance']['observations'][0]['value'], r['metrics']['metrics']['quantile_metric']['observations'][0]['value'])


print(sorted_results[0]['trial_id'])
print(sorted_results[0]['path'])
print(sorted_results[0]["hyperparameters"]["values"])
#print(sorted_results[0]["metrics"]["metrics"]['quantile_distance']['observations'][0]['value'])
print(sorted_results[0]["metrics"]["metrics"]['quantile_metric']['observations'][0]['value'])
#print(sorted_results[0]["metrics"]["metrics"]['val_quantile_distance']['observations'][0]['value'])
print(sorted_results[0]["metrics"]["metrics"]['val_quantile_metric']['observations'][0]['value'])

num_units_1 = sorted_results[0]["hyperparameters"]["values"]["num_units_1"]
num_units_2 = sorted_results[0]["hyperparameters"]["values"]["num_units_2"] 
num_units_3 = sorted_results[0]["hyperparameters"]["values"]["num_units_3"]
l2_reg = sorted_results[0]["hyperparameters"]["values"]["l2_reg"] 
dropout = sorted_results[0]["hyperparameters"]["values"]["dropout"] 
learning_rate = sorted_results[0]["hyperparameters"]["values"]["learning_rate"] 

test_dist = 0# #round(sorted_results[0]["metrics"]["metrics"]['quantile_distance']['observations'][0]['value'][0],3)
test_quan =  round(sorted_results[0]["metrics"]["metrics"]['quantile_metric']['observations'][0]['value'][0],3)
val_dist = 0#round(sorted_results[0]["metrics"]["metrics"]['val_quantile_distance']['observations'][0]['value'][0],3)
val_quan = round(sorted_results[0]["metrics"]["metrics"]['val_quantile_metric']['observations'][0]['value'][0],3)

print (num_units_1, num_units_2, num_units_3, dropout, learning_rate,  l2_reg, val_dist, test_dist, val_quan, test_quan, sep=" & ", end="\\")
