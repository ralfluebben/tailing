# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Derived from https://www.tensorflow.org/tutorials/structured_data/time_series
# Copyright 2023 Ralf Lübben, original Copyright 2019 The TensorFlow Authors.

import os
import gc
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import argparse
import sys
import math
import json
import metrics
import windowgenerator
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("-t","--model_type", help="Model", type=str, required=True, choices=["Dense", "LSTM", "Feedback"])
parser.add_argument("-s","--input_start", help="Packet to start observation", type=int, required=True)
parser.add_argument("-w","--input_width", help="Trainlength used for prediction", type=int, required=True)
parser.add_argument("-l","--label_width", help="Output prediction length", type=int, required=True)
parser.add_argument("-r","--learning_rate", help="Learning rate to use: 0.0 meand adaptaive lr", type=float, required=True)
parser.add_argument("-d","--data_tag", help="Input data", type=str, required=True)
parser.add_argument("-f","--input_type", help="Features: delay_only, delay_iat_short, delay_iat_long", type=str, required=True)
parser.add_argument("-m","--model_trial_id", help="Trained model use for plot timeseries", type=str)
parser.add_argument("-u","--tuner", help="Tuner to use", type=str, required=True, choices=["RandomSearch", "Hyperband"])
parser.add_argument("-j","--job_id", help="Jobid", type=int, required=False)
args = parser.parse_args()

input_start = args.input_start
input_width = args.input_width
label_width = args.label_width
data_tag = args.data_tag
learning_rate_argv = args.learning_rate
input_type = args.input_type
modeltype = args.model_type
tuner = args.tuner


jobid = args.job_id
print("Jobid", jobid)

if args.job_id is not None:
    jobid = args.job_id - 1
    p = subprocess.Popen(["/usr/bin/nvidia-smi", "-L"],stdout=subprocess.PIPE)
    devs = p.stdout.readlines()
    migs = []
    for l in devs:
        if "MIG" in l.decode('utf-8'):
            migs.append(l.decode('utf-8').strip())
    nofmigs = len(migs)
    migs2=[]
    mid=jobid%nofmigs
    for m in migs:
        migs2.append(m.split(":")[2].replace(')','').strip())

    os.environ["CUDA_VISIBLE_DEVICES"] = migs2[mid]
    print(os.environ["CUDA_VISIBLE_DEVICES"])

    
import tensorflow as tf
from tensorboard.plugins.hparams import api as tf_hp
from tensorflow import keras

from keras_tuner import HyperParameters
from keras_tuner.tuners import Hyperband
from keras_tuner import HyperModel
from keras_tuner.tuners import RandomSearch
import keras_tuner
print(tf.__version__)

gpus = tf.config.list_physical_devices('GPU')
for g in gpus:
  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.experimental.set_memory_growth(g, True)
    #tf.config.experimental.set_virtual_device_configuration(
    #    gpus[0],
    #    [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=3500)])
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)    

data_tags=None
begin_of_delays = 3000
use_testing_parameters = False
if data_tag == "mm1_75" or data_tag == "mw1_75" or data_tag == "ww1_75" or data_tag == "ARTA" or data_tag == "CHEP" or  data_tag == "Dist" or data_tag == "MAP" or data_tag == "ar" or data_tag == "periodic_on_off" or data_tag == "on_off_100_bf" or data_tag == "on_off_50_bf": 
  data_filename = 'data/data_' + data_tag + '.npy'
  data = np.load(data_filename)
  data=data[0:300000,:] # memory exhaution on workstation with 16GB RAM
elif data_tag == "testing":
  data_filename = 'data/data_' + data_tag + '.npy'
  data = np.load(data_filename)
  use_testing_parameters = True
elif data_tag == "mix":
  nof_samples = 100000
  data = None
  load = [ "75" ]
  dist = [ "mm1", "mw1", "ww1" ]
  for lo in load:
      for di in dist:
          data_tag_tmp = di + "_" + lo
          data_filename = './data/data_' + data_tag_tmp + '.npy'
          data_tmp = np.load(data_filename)
          data_tmp=data_tmp[0:nof_samples,:]
          data_tags_tmp=np.empty((data_tmp.shape[0],1), dtype='U8')
          data_tags_tmp[:,:]=data_tag_tmp
          if data is None:
              data = data_tmp
              data_tags = data_tags_tmp
          else:
              data = np.concatenate([data, data_tmp], axis=0)
              data_tags = np.concatenate([data_tags, data_tags_tmp], axis=0)
          del data_tmp
          print(data.shape)
  gc.collect()
  dist = [ "ARTA", "CHEP", "Dist", "MAP" ]
  for di in dist:
      data_tag_tmp = di
      data_filename = './data/data_' + data_tag_tmp + '.npy'
      data_tmp = np.load(data_filename)
      data_tmp=data_tmp[0:nof_samples,:]
      data_tags_tmp=np.empty((data_tmp.shape[0],1), dtype='U8')
      data_tags_tmp[:,:]=data_tag_tmp
      data = np.concatenate([data, data_tmp], axis=0)
      data_tags = np.concatenate([data_tags, data_tags_tmp], axis=0)
      del data_tmp
      print(data.shape)
  gc.collect()
elif data_tag == "mix1":
  nof_samples = 100000
  data = None
  load = [ "75" ]
  dist = [ "mm1", "mw1", "ww1" ]
  for lo in load:
      for di in dist:
          data_tag_tmp = di + "_" + lo
          data_filename = './data/data_' + data_tag_tmp + '.npy'
          data_tmp = np.load(data_filename)
          data_tmp=data_tmp[0:nof_samples,:]
          data_tags_tmp=np.empty((data_tmp.shape[0],1), dtype='U8')
          data_tags_tmp[:,:]=data_tag_tmp
          if data is None:
              data = data_tmp
              data_tags = data_tags_tmp
          else:
              data = np.concatenate([data, data_tmp], axis=0)
              data_tags = np.concatenate([data_tags, data_tags_tmp], axis=0)
          del data_tmp
          print(data.shape)
  gc.collect()
elif data_tag == "mix2":
  nof_samples = 40000
  data = None
  load = [ "75", "50", "25" ]
  dist = [ "mm1", "mw1", "ww1" ]
  for lo in load:
      for di in dist:
          data_tag_tmp = di + "_" + lo
          data_filename = './data/data_' + data_tag_tmp + '.npy'
          data_tmp = np.load(data_filename)
          data_tmp=data_tmp[0:nof_samples,:]
          data_tags_tmp=np.empty((data_tmp.shape[0],1), dtype='U8')
          data_tags_tmp[:,:]=data_tag_tmp
          if data is None:
              data = data_tmp
              data_tags = data_tags_tmp
          else:
              data = np.concatenate([data, data_tmp], axis=0)
              data_tags = np.concatenate([data_tags, data_tags_tmp], axis=0)
          del data_tmp
          print(data.shape)
  gc.collect()
elif data_tag == "mix3":
  nof_samples = 100000
  data = None
  load = [ "75", "50", "25" ]
  dist = [ "ww1" ]
  for lo in load:
      for di in dist:
          data_tag_tmp = di + "_" + lo
          data_filename = './data/data_' + data_tag_tmp + '.npy'
          data_tmp = np.load(data_filename)
          data_tmp=data_tmp[0:nof_samples,:]
          data_tags_tmp=np.empty((data_tmp.shape[0],1), dtype='U8')
          data_tags_tmp[:,:]=data_tag_tmp
          if data is None:
              data = data_tmp
              data_tags = data_tags_tmp
          else:
              data = np.concatenate([data, data_tmp], axis=0)
              data_tags = np.concatenate([data_tags, data_tags_tmp], axis=0)
          del data_tmp
          print(data.shape)
  gc.collect()
elif data_tag == "mlab":
  data_filename = 'data/data_' + data_tag + '.npy'
  data = np.load(data_filename)
  data = data.T
  begin_of_delays = 799
  data=data[0:300000,:]

gc.collect()

np.random.seed(42)
indices = np.arange(data.shape[0])
np.random.shuffle(indices)
data=data[indices]
if data_tags is not None:
  data_tags=data_tags[indices]

data=np.reshape(data,(-1,begin_of_delays,2), order='F')
print(data.shape)


import xarray as xr
df=xr.DataArray(data, dims=["measurement","packetnum","values"])
df["measurement"]=list(range(0,data.shape[0]))
df["packetnum"]=list(range(0,data.shape[1]))
df["values"]=["iat", "delay"]

del data
n = len(df)

indices_train_1 = 0
indices_train_2 = int(n*0.8)
indices_val_1 = indices_train_2
indices_val_2 = int(n*0.9)
indices_test_1 = indices_val_2
indices_test_2 = df.shape[0]


train_df = df[indices_train_1:indices_train_2]
val_df = df[indices_val_1:indices_val_2]
test_df = df[indices_test_1:indices_test_2]
if data_tags is not None:
  train_df.attrs["tag"]=data_tags[indices_train_1:indices_train_2]
  val_df.attrs["tag"]=data_tags[indices_val_1:indices_val_2]
  test_df.attrs["tag"]=data_tags[indices_test_1:indices_test_2]
del df

feature_columns=["delay"]
future_iats=False

if input_type == "delay_only":
  feature_columns=["delay"]
  future_iats=False
  num_features = 1
elif input_type == "delay_iat_long":
  feature_columns=["iat", "delay"]
  future_iats=True
  num_features = 2  
elif input_type == "delay_iat_short":
  feature_columns=["iat", "delay"]
  future_iats=False
  num_features = 2
else:
  sys.exit("Wrong input")

w1 = windowgenerator.WindowGenerator(input_start=input_start, input_width=input_width, label_width=label_width, shift=0, label_columns=['delay'], feature_columns=feature_columns, train_df=train_df, val_df=val_df, test_df=test_df, future_iats=future_iats)

i_label = feature_columns.index("delay")


del train_df
del val_df
del test_df

#### LOSS Function

def tilted_loss(y_true, y_pred):
  q = q_alpha
  e = y_true - y_pred
  tl=tf.stack([q * e, (q - 1) * e])
  e_max=tf.math.reduce_max(tl,axis=0,keepdims=True)
  return tf.reduce_mean(e_max)

#### NN Models: Dense, LSTM, Feedback, ...

class FeedBack(tf.keras.Model):
  def __init__(self, units, out_steps):
    super().__init__()
    self.out_steps = out_steps
    self.units = units
    self.lstm_cell = tf.keras.layers.LSTMCell(units)
    # Also wrap the LSTMCell in an RNN to simplify the `warmup` method.
    self.lstm_rnn = tf.keras.layers.RNN(self.lstm_cell, return_state=True)
    self.dense = tf.keras.layers.Dense(num_features)

  def warmup(self, inputs):
    # inputs.shape => (batch, time, features)
    # x.shape => (batch, lstm_units)
    x, *state = self.lstm_rnn(inputs)

    # predictions.shape => (batch, features)
    prediction = self.dense(x)
    return prediction, state
  
  def call(self, inputs, training=None):
    # Use a TensorArray to capture dynamically unrolled outputs.
    predictions = []
    # Initialize the lstm state
    prediction, state = self.warmup(inputs)

    # Insert the first prediction
    predictions.append(prediction)

    # Run the rest of the prediction steps
    for n in range(1, self.out_steps):
      # Use the last prediction as input.
      x = prediction
      # Execute one lstm step.
      x, state = self.lstm_cell(x, states=state,
                                training=training)
      # Convert the lstm output to a prediction.
      prediction = self.dense(x)
      # Add the prediction to the output
      predictions.append(prediction)

    # predictions.shape => (time, batch, features)
    predictions = tf.stack(predictions)
    # predictions.shape => (batch, time, features)
    predictions = tf.transpose(predictions, [1, 0, 2])
    return predictions[:,:,i_label]

class FeedbackModel(HyperModel):
  def build(self, hp):
    print("### Build FeedbackModel model ###")
    _num_units_1 = hp.get('num_units_1')
    _learning_rate = hp.get('learning_rate')
    _optimizer = hp.get('optimizer')

    model = FeedBack(_num_units_1, label_width)

    if _optimizer == 'adam':
      opt = tf.keras.optimizers.Adam(learning_rate= _learning_rate)
    
    model.compile(
        optimizer=opt,
        loss=[tilted_loss],
        metrics=[metrics.QuantileMetric(), keras.metrics.MeanAbsoluteError()]
    )
    return model

class MyLSTMModel(HyperModel):
  def build(self, hp):
    print("### Build LSTM model ###")
    _num_units_1 = hp.get('num_units_1')
    _learning_rate = hp.get('learning_rate')
    _optimizer = hp.get('optimizer')

    model = tf.keras.Sequential([
      tf.keras.layers.LSTM(_num_units_1, return_sequences=False),
      tf.keras.layers.Dense(label_width)
    ])

    if _optimizer == 'adam':
      opt = tf.keras.optimizers.Adam(learning_rate= _learning_rate)
    
    model.compile(
        optimizer=opt,
        loss=[tilted_loss],
        metrics=[metrics.QuantileMetric(), keras.metrics.MeanAbsoluteError()]
    )
    return model

class MyDenseModel(HyperModel):
  def build(self, hp):
    _num_units_1 = hp.get('num_units_1')
    _num_units_2 = hp.get('num_units_2')
    _num_units_3 = hp.get('num_units_3')
    
    _learning_rate = hp.get('learning_rate')
    _dropout = hp.get('dropout')
    _l2_reg = hp.get('l2_reg')
    _optimizer = hp.get('optimizer')

    model = tf.keras.models.Sequential([
        tf.keras.layers.Masking(mask_value=0),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(_num_units_1, activation='relu',kernel_regularizer=tf.keras.regularizers.l2(_l2_reg)),
    ])

    # Add dropout only is greater 0.0
    if _dropout > 0.0:
        model.add(tf.keras.layers.Dropout(_dropout))

    # Add additional layer if > 0.0
    if _num_units_2 > 0:
        model.add(tf.keras.layers.Dense(_num_units_2, activation='relu',kernel_regularizer=tf.keras.regularizers.l2(_l2_reg)))
        if _num_units_3 > 0:
          model.add(tf.keras.layers.Dense(_num_units_3, activation='relu',kernel_regularizer=tf.keras.regularizers.l2(_l2_reg)))

    model.add(tf.keras.layers.Dense(label_width))

    if _optimizer == 'adam':
      opt = tf.keras.optimizers.Adam(learning_rate= _learning_rate)
    
    model.compile(
        optimizer=opt,
        loss=[tilted_loss],
        metrics=[metrics.QuantileMetric(), keras.metrics.MeanAbsoluteError()]
    )
    return model

if modeltype == "Dense":
  hypermodel = MyDenseModel(tunable=False)
  project_name = "Dense" + data_tag + "_" + input_type + "_" + str(input_width) + "_" + str(input_start) +  "_" + str(label_width) + "_" + str(learning_rate_argv)
elif modeltype == "LSTM":
  hypermodel = MyLSTMModel(tunable=False)
  project_name = "LSTM" + data_tag + "_" + input_type + "_" + str(input_width) + "_" + str(input_start) +  "_" + str(label_width) + "_" + str(learning_rate_argv)
elif modeltype == "Feedback":
  hypermodel = FeedbackModel(tunable=False)
  project_name = "FB" + data_tag + "_" + input_type + "_" + str(input_width) + "_" + str(input_start) +  "_" + str(label_width) + "_" + str(learning_rate_argv)

########### Hyperparameters

batch_size = 2048
q_alpha = 0.95
min_delta = 0.005

if modeltype == "Dense":
  hps = { "hp_learning_rate" : [learning_rate_argv],
  "hp_num_units_1" : [30, 40, 100, 200, 1200],
  "hp_num_units_2" : [0, 10, 20, 30, 40],
  "hp_num_units_3" : [0, 10, 20, 30],
  "hp_l2_reg" : [0.01, 0.001],
  "hp_optimizer" : ['adam'],
  "hp_dropout" : [0.0, 0.5],
  }
  epochs = 600
  max_hps = 30
  use_testing_parameters = False
  # define testing parameters for fast evaluation
  if use_testing_parameters == True:
    hps = { "hp_learning_rate" : [0.0],
    "hp_num_units_1" : [200],
    "hp_num_units_2" : [50],
    "hp_num_units_3" : [10],
    "hp_l2_reg" : [0.001],
    "hp_optimizer" : ['adam'],
    "hp_dropout" : [0.5]
    }
    epochs = 10
    max_hps = 1
elif modeltype == "LSTM":
  hps = { "hp_learning_rate" : [learning_rate_argv],
  "hp_num_units_1" : [5, 10, 20, 40, 70, 120, 200],
  "hp_num_units_2" : [0],
  "hp_num_units_3" : [0],
  "hp_l2_reg" : [0.001],
  "hp_optimizer" : ['adam'],
  "hp_dropout" : [0.0],
  }
  epochs = 200
  max_hps = 7
elif  modeltype == "Feedback":
  hps = { "hp_learning_rate" : [learning_rate_argv],
  "hp_num_units_1" : [5, 10, 20, 40, 70],
  "hp_num_units_2" : [0],
  "hp_num_units_3" : [0],
  "hp_l2_reg" : [0.0],
  "hp_optimizer" : ['adam'],
  "hp_dropout" : [0.0],
  }
  epochs = 200
  max_hps = 5

# Calculate max trials for random search, for greedy approach
max_trials = 1
for key in hps:
  max_trials *= len(hps[key])
print("Max trials: " + str(max_trials))

hp = HyperParameters()
hp.Choice('learning_rate', values=hps["hp_learning_rate"])
hp.Choice('num_units_1', values=hps["hp_num_units_1"])
hp.Choice('num_units_2', values=hps["hp_num_units_2"])
hp.Choice('num_units_3', values=hps["hp_num_units_3"])
hp.Choice('l2_reg', values=hps["hp_l2_reg"])
hp.Choice('optimizer', values=hps["hp_optimizer"])
hp.Choice('dropout', values=hps["hp_dropout"])

print(hp.get_config()['values'])

############################

logdir = "results/" + project_name

def scheduler(epoch, lr):
  if epoch < epochs/4:
    return 0.01
  elif epoch < 2*epochs/4:
    return 0.001
  else:
    return 0.0001

if tuner == "RandomSearch":
  class MyTuner(RandomSearch):
    def __init__(self,
                 hypermodel,
                 objective,
                 max_trials,
                 seed=None,
                 hyperparameters=None,
                 tune_new_entries=True,
                 allow_new_entries=True,
                 **kwargs):
      super(MyTuner, self).__init__(
              hypermodel,
              objective,
              max_trials,
              seed,
              hyperparameters,
              tune_new_entries,
              allow_new_entries,
              **kwargs)

    def run_trial(self, trial, *fit_args, **fit_kwargs):
        gc.collect()
        values = trial.hyperparameters.get_config()['values']
        lr = trial.hyperparameters.get_config()['values']['learning_rate']
        if math.isclose(lr, 0.0, abs_tol=1e-6):
          lr_scheduler = keras.callbacks.LearningRateScheduler(scheduler)
          print("ADD:" + str(type(lr_scheduler)))
          fit_kwargs['callbacks'].append(lr_scheduler)
        else: # add early stopping, if learning rate is fixed
          early_stop = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10, min_delta=min_delta)
          print("ADD:" + str(type(early_stop)))
          fit_kwargs['callbacks'].append(early_stop)
        
        # work around to skip invalid configurations 
        if values['num_units_3'] > 0.1:
          if values['num_units_2'] < 0.1:
            terminate = metrics.TerminateImmediatelyCallback()
            print("ADD:" + str(type(terminate)))
            fit_kwargs['callbacks'].append(terminate)
              
        super().run_trial(trial, *fit_args, **fit_kwargs)

        # remove callbacks added earlier, else they remain between runs
        to_be_removed = []
        for c in fit_kwargs['callbacks']:
          if isinstance(c, metrics.TerminateImmediatelyCallback):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
          elif isinstance(c, tf.keras.callbacks.EarlyStopping):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
          elif isinstance(c, keras.callbacks.LearningRateScheduler):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
        for r in to_be_removed:
          fit_kwargs['callbacks'].remove(r)

  tuner = MyTuner(
    hypermodel,
    hyperparameters=hp,
    objective=keras_tuner.Objective('loss', direction="min"),
    max_trials=epochs,
    directory='results',
    project_name=project_name,
    tune_new_entries=False,  # only use HPs listed before
    allow_new_entries=False
  )
if tuner == "Hyperband":
  class MyTuner(Hyperband):
    def __init__(self,
                 hypermodel,
                 objective,
                 max_epochs,
                 factor=3,
                 hyperband_iteration=1,
                 seed=None,
                 hyperparameters=hp,
                 tune_new_entries=True,
                 allow_new_entries=True,
                 max_hp_choices=None,
                 **kwargs):
      super(MyTuner, self).__init__(
              hypermodel,
              objective,
              max_epochs,
              factor,
              hyperband_iteration,
              seed,
              hyperparameters,
              tune_new_entries,
              allow_new_entries,
              max_hp_choices,
              **kwargs)
    def run_trial(self, trial, *fit_args, **fit_kwargs):
        gc.collect()
        values = trial.hyperparameters.get_config()['values']
        lr = trial.hyperparameters.get_config()['values']['learning_rate']
        if math.isclose(lr, 0.0, abs_tol=1e-6):
          lr_scheduler = keras.callbacks.LearningRateScheduler(scheduler)
          print("ADD:" + str(type(lr_scheduler)))
          fit_kwargs['callbacks'].append(lr_scheduler)
        else: # add early stopping, if learning rate is fixed
          early_stop = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10, min_delta=min_delta)
          print("ADD:" + str(type(early_stop)))
          fit_kwargs['callbacks'].append(early_stop)
        
        # work around to skip invalid configurations 
        if values['num_units_3'] > 0.1:
          if values['num_units_2'] < 0.1:
            terminate = metrics.TerminateImmediatelyCallback()
            print("ADD:" + str(type(terminate)))
            fit_kwargs['callbacks'].append(terminate)
              
        res = super().run_trial(trial, *fit_args, **fit_kwargs)

        # remove callbacks added earlier, else they remain between runs
        to_be_removed = []
        for c in fit_kwargs['callbacks']:
          if isinstance(c, metrics.TerminateImmediatelyCallback):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
          elif isinstance(c, tf.keras.callbacks.EarlyStopping):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
          elif isinstance(c, keras.callbacks.LearningRateScheduler):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
        for r in to_be_removed:
          fit_kwargs['callbacks'].remove(r)

        return res

  tuner = MyTuner(
    hypermodel,
    hyperparameters=hp,
    max_epochs=epochs,
    objective='loss',
    directory='results',
    project_name=project_name,
    tune_new_entries=False,  # only use HPs listed before
    allow_new_entries=False,
    max_hp_choices=max_hps
  )

if not args.model_trial_id:
  x,y=w1.train
  x_val, y_val = w1.val
  tuner.search(x,y,
    epochs=epochs,
    batch_size=batch_size,
    validation_data=(x_val,y_val), use_multiprocessing=True, callbacks=[])
elif args.model_trial_id: # Evaluate existing trial with given id
  trial_fn = logdir + "/trial_" + args.model_trial_id +"/trial.json"
  print(trial_fn)
  with open(trial_fn) as json_file:
    netword_layout = json.load(json_file)
  num_units_1 = netword_layout["hyperparameters"]["values"]["num_units_1"]
  num_units_2 = netword_layout["hyperparameters"]["values"]["num_units_2"] 
  num_units_3 = netword_layout["hyperparameters"]["values"]["num_units_3"]
  l2_reg = netword_layout["hyperparameters"]["values"]["l2_reg"] 
  dropout = netword_layout["hyperparameters"]["values"]["dropout"] 
  learning_rate = netword_layout["hyperparameters"]["values"]["learning_rate"] 

  trial = keras_tuner.engine.trial.Trial.load(trial_fn)
  model = tuner.load_model(trial)
  train_loss, train_q , train_dist = model.evaluate(w1.train[0], w1.train[1], batch_size=batch_size)
  train_dist = train_dist * w1.get_label_norm()+w1.label_mean[0]
  model.summary()
  val_loss, val_quan , val_dist = model.evaluate(w1.val[0], w1.val[1], batch_size=batch_size)
  val_dist = val_dist * w1.get_label_norm()+w1.label_mean[0]
  test_loss, test_quan , test_dist = model.evaluate(w1.test[0], w1.test[1], batch_size=batch_size)
  test_dist = test_dist * w1.get_label_norm()+w1.label_mean[0]

  print("Result: ", num_units_1, num_units_2, num_units_3, dropout, learning_rate,  l2_reg, round(val_dist,3), round(test_dist, 3), round(val_quan, 3), round(test_quan, 3), sep=" & ", end="\\\\\n")

  #w1.stats(model)
  #w1.plot(model,fig_extension=project_name)
  w1.plot_mean(model,fig_extension=project_name)
  w1.plot_single(model,fig_extension=project_name)
  w1.stats_per_tag(model,fig_extension=project_name)
  #w1.plot_ts_delay(fig_extension=project_name)
  #w1.plot_iat_delay(fig_extension=project_name)
  
