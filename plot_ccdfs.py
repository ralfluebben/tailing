# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import pandas
import matplotlib.pyplot as plt
import numpy as np
from statsmodels.graphics.tsaplots import plot_acf#
import statsmodels

data_tag="mm1"
data_tag="mw1"
data_tag="ww1"
data_tag="mlab"

iat_start=0
iat_end=50
begin_of_delays = 3000
div=1e3
lags=100

load = [ "25", "50", "75" ]
for lo in load:

  if data_tag == "mm1":
    data = np.load("data_" + data_tag + "_" + lo + '.npy')
  elif data_tag == "mw1":
    data = np.load("data_" + data_tag + "_" + lo + '.npy')
  elif data_tag == "mlab":
    data = np.load('data_mlab.npy').T
    begin_of_delays = 800
    lags = 250
    div = 1.0
  else:
    data = np.load("data_" + data_tag + "_" + lo + '.npy')
  #

  idelays = [10, 50 , 100, 500]
  fig = plt.figure()
  ax = fig.add_subplot(1,1,1)
  ax.set_yscale('log')
  #ax.set_xscale('log')

  leng = data.shape[1]
  for i in idelays:
    l=str(i) + " " + lo
    s=i+begin_of_delays
    ax.hist(data[:,s]/div, bins=10000, density=True, histtype='step', cumulative=-1, label=l )
    q = np.quantile(data[:,s]/div, 0.95)
    ax.plot([0, q], [0.05, 0.05], '--')
    ax.plot([q, q], [0, 0.05], '--')
  ax.legend()
  ax.set_xlabel("delay [s]")
  ax.set_ylabel("$P[delay > x] \leq 1-q$")
  fn="ccdf_" + data_tag + "_" + lo + ".pdf"
  plt.savefig(fn)
  plt.close()

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
for l in load:
  if data_tag == "mm1":
    data = np.load("data_" + data_tag + "_" + l + '.npy')
  elif data_tag == "mw1":
    data = np.load("data_" + data_tag + "_" + l + '.npy')
  elif data_tag == "mlab":
    data = np.load('data_mlab.npy').T
    begin_of_delays = 800
  else:
    data = np.load("data_" + data_tag + "_" + l + '.npy')

  macf = np.zeros((data.shape[0]-10 , lags+1))

  for i in range(data.shape[0]-10):
    print(i)
    #macf[i,:]=statsmodels.tsa.stattools.acf(data[i, begin_of_delays+1000:begin_of_delays+9000], fft=True, nlags=lags)
    macf[i,:]=statsmodels.tsa.stattools.acf(data[i, begin_of_delays:begin_of_delays+800], fft=True, nlags=lags)
 
  print(np.mean(macf, axis=0))

  
  ax.plot(np.mean(macf, axis=0))
  ax.set_xlabel("lab")
  ax.set_ylabel("mean autocorrlation")
fn="acf_" + data_tag + ".pdf"
plt.savefig(fn)

## plot single acfs

for l in load:
  if data_tag == "mm1":
    data = np.load("data_" + data_tag + "_" + l + '.npy')
  elif data_tag == "mw1":
    data = np.load("data_" + data_tag + "_" + l + '.npy')
  elif data_tag == "mlab":
    data = np.load('data_mlab.npy').T
    begin_of_delays = 800
    print(data.shape)
  else:
    data = np.load("data_" + data_tag + "_" + l + '.npy')

  macf = np.zeros((data.shape[0]-10 , lags+1))

  for i in range(data.shape[0]-10): 
    if (i % 10000) == 0:
      macf[i,:]=statsmodels.tsa.stattools.acf(data[i, begin_of_delays:begin_of_delays+800], fft=True, nlags=lags)
      fig, axs = plt.subplots(2)
      axs[0].plot(data[i, begin_of_delays:begin_of_delays+800])
      axs[1].plot(macf[i,:])
      axs[1].set_xlabel("lab")
      axs[1].set_ylabel("autocorrlation")
      fn="acf_" + data_tag + "_" + str(i) + ".pdf"
      plt.savefig(fn)
      plt.close()

  

  
  
