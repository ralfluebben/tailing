# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import numpy as np
import sys
try:

    r=np.genfromtxt(sys.argv[2],delimiter='\t')
    s=np.genfromtxt(sys.argv[3],delimiter='\t')

    if r.shape[0] > 800 and s.shape[0] > 10:
        ts = np.empty((r.shape[0]-1,4))
        ts[:,0:2] = r[1:,:]
        ts[:,2:4] = np.NaN
        for i in range(1,r.shape[0]):
            s_idx = np.argwhere(s[:,0]<=(r[i,0])-1)[-1]
            
            delay = r[i,1] - s[s_idx,1]

            ts[i-1,2:4] = s[s_idx,:]
            #print (delay)
            if delay < 0.0001:
                # so this will skip the measurement if the seq is sent again after the ack arrives, e.g. zero window probe
                print (delay)
                print ("Error!!!!!!!!!!!!!!!!!!!!!!!!!!")
                raise Exception('neg. delay')
        
        filename = sys.argv[1] + ".npy"
        np.save(filename,ts)
        print(filename)
    else:
        print("only " + str(r) + " acks")

except Exception as e: print(e)
    