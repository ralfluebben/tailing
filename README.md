# Goal
Prediction of future delays based on prior delays and arrival traffic information.

Details are described in the paper *TAILING: Tail Distribution Forecasting of Packet Delays using Quantile Regression Neural Networks* presented at IEEE ICC conference 2023.

# Input Data

The training, validatuin, and test data is extracted from a queueing simualtion performed by the use of Omnet++ and RTT measurements from the measurement lab (M-LAB), https://www.measurementlab.net/data/, dataset.

The data is expected to be in the subfolder `data` as a `.npy` file, with appropriate nameing, e.g. for the M/M/1 queueing system with 75% utilization, the expected file name is `data_mm1_75.npy`. For the expected file names have a look at `window.py`. For the file format have a look at `aggregate_omnet_results.py`, `train2000_from_csv_parallel_with_TA.py` and `aggregate_mlab.py`. For each trace the file contains the related delay values followed by the interarrivaltimes.

## Omnet++ one way delay simulations

From the Omnet++ queueing simulation arrival and departure timestamps as well as delay measurements are collected.

Different queueing system with various utilization values are conducted:

- mm1
  - 25
  - 50 
  - 75
- mw1
  - 25
  - 50 
  - 75
- ww1
  - 25
  - 50 
  - 75
- Markov on off T=100
- Markov on off T=50
- Markov on off with pause
- Mix of mm1/mw1/ww1 with utilization 75
- Mix of utilization 25/50/75

The script to perform the simulation is `run_omnetpp.sh`. The source code of the simulator is available at https://gitlab.com/ralfluebben/omnetpp/-/tree/feature/simulation_mm1_weibull_queue, therefore the sample fifo is modified for the different soures and services. For the different configurations see https://gitlab.com/ralfluebben/omnetpp/-/blob/feature/simulation_mm1_weibull_queue/samples/fifo/omnetpp.ini .

To preprocess the data to be used for training the neural network, user `aggregate_omnet_results.py`.

Preprocessed input data used for the ICC paper `TAILING: Tail Distribution Forecasting of Packet Delays using Quantile Regression Neural Networks` is available at https://www2.wi.hs-flensburg.de/opendata/ . Please cite the paper if you use the data. Additional data as mentioned above is available on request.

## MLab RTT data

Download pcap data files with `download_mlab.sh`, it downloads, extracts, and preprocesses the packet trace, `aggregate_mlab.py` aggregated the traces into one file for training.

# Prerequirements
- Tensorflow 2.8
- costumized/bugfixes kerastuner
```
pip3 install tensorflow xarray numpy pandas seaborn
pip3 install git+https://github.com/ralfluebben/keras-tuner.git
```

# Usage
usage: window.py [-h] -t {Dense,LSTM,Feedback} -s INPUT_START -w INPUT_WIDTH -l LABEL_WIDTH -r LEARNING_RATE -d DATA_TAG -f INPUT_TYPE [-m MODEL_TRIAL_ID] -u {RandomSearch,Hyperband} [-j JOB_ID]

```
python3 window.py -t LSTM -s 399 -w 1 -l 200 -r 0.0 -d mix1 -f delay_only -u Hyperband -j 10
```

With `get_best_tuner_model.py` get the best training result and evaluate with

```
python3 window.py -t LSTM -s 399 -w 1 -l 200 -r 0.0 -d mix1 -f delay_only -u Hyperband -j 10 -m 0015
```
if 0015 is your best result.