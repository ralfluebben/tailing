# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

#..gsutil cp gs://archive-measurement-lab/ndt/pcap/2020/08/28/*pcap-mlab1-ham02* ./
#find ./ -name  "20200827*" -exec tar xvzf {} \;
#`find ./ -size +10k -name "*.pcap.gz"`
f=$1
echo $f
echo "################"
bytes_received=`tshark -r ${f} -Y "tcp.dstport == 443" -e tcp.len -Tfields | awk '{ sum += $0 } END { print sum }'`
bytes_sent=`tshark -r ${f} -Y "tcp.srcport == 443" -e tcp.len -Tfields | awk '{ sum += $0 } END { print sum }'`
echo "Received: $bytes_received"
echo "Send: $bytes_sent"
fb=`basename ${f} .pcap.gz`
if [ "$bytes_sent" == "" ]; then
  echo "No bytes sent"
  exit 0
fi
if [ "$bytes_received" == "" ]; then
  echo "No bytes received"
  exit 0
fi
if [ "$bytes_sent" -gt "$bytes_received" ]; then
  echo "Download"
  rf="${fb}_recv.csv"
  sf="${fb}_send.csv"
  tshark -r ${f} -Y "tcp.dstport == 443" -e tcp.ack_raw -e frame.time_epoch -Tfields > $rf
  tshark -r ${f} -Y "tcp.srcport == 443" -e tcp.seq_raw -e frame.time_epoch -Tfields > $sf
  python3 ~/git/dsl_prediction/extract_timestamp_mlab.py $fb $rf $sf
  rm $sf
  rm $rf
else
  echo "Upload"
fi
echo "################"
