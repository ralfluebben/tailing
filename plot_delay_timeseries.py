# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import matplotlib.pyplot as plt
import numpy as np

data_tag="m_m_1"
data_tag="m_w_1"
data_tag="tcp"
data_tag="mlab"
data_tag="data_mm1_75.npy"
begin_of_delays = 3000
d = None
if data_tag == "data_mm1_75.npy":
  data = np.load('data/' + data_tag)
  print(data.shape)
  rho = 0.75
  mu = 1/0.05
  Es = 1.0/(1-rho)*1.0/mu
  epsilon=0.95
  a=-np.log(1-epsilon)
  d=a*Es
elif data_tag == "m_w_1":
  data = np.load('data/' + data_tag + '/data.npy')
elif data_tag == "tcp":
  data = np.load('/home/ralf/data_ns3.nparr.npy')
elif data_tag == "mlab":
    data = np.load('data/data_mlab.npy').T
    begin_of_delays = 800

delays = data[:,begin_of_delays:]
print(delays.shape)
x_len = delays.shape[1]
i1=int(x_len/2)
i2=i1+1
x1 = range(0,i1-100)
x2 = range(i1-100,i1)
x3 = range(i1,i1+200)
x4 = range(i1+200,x_len)

fig = plt.figure()
nof=1
for i in range(nof):
    c=0.1+1.0/(2.0*(i+1))
    plt.plot(x1,delays[i,0:i1-100].T, color=(c,c,c),label="delays")
    plt.plot(x2,delays[i,i1-100:i1].T, color=(c,+0.2,c),label="delays - input")
    plt.plot(x3,delays[i,i1:i1+200].T, color=(c+0.2,c,c),label="delays - output")
    plt.plot(x4,delays[i,i1+200:].T, color=(c,c,c))

q=np.quantile(delays, 0.95, axis=0)
plt.plot(range(0, x_len), q, 'r-', label="empi. quantile")
if d is not None:
    plt.plot([0, x_len], [d, d], 'g--', label="anal. quantile")
plt.legend()
fn="ts_" + data_tag + ".pdf"
plt.savefig(fn)

