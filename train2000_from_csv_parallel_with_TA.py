# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import pandas as pd
import numpy as np
import os
from multiprocessing import Pool
import sys

i_start=0
i_end=3000
file_idx_start = int(sys.argv[1])
file_idx_end   = int(sys.argv[2])
config = sys.argv[3]

def get_data(i):
  print(i)
  global i_start
  global i_end
  global file_idx_start
  csv_filename='{}-#{}.csv'.format(config, i+file_idx_start)
  routing = pd.read_csv(csv_filename)
  vectors = routing[routing.type=='vector']
  TAvec = vectors[vectors.name == 'TA:vector'].iloc[0]
  TDvec = vectors[vectors.name == 'TD:vector'].iloc[0]
  TAvecnp = np.fromstring(TAvec.vecvalue, dtype=float, sep=' ')
  TDvecnp = np.fromstring(TDvec.vecvalue, dtype=float, sep=' ')
  delays=TDvecnp-TAvecnp
  interarrivaltimes=TAvecnp[1:len(TAvecnp)]-TAvecnp[0:len(TAvecnp)-1]
  d_tmp=np.concatenate([interarrivaltimes[i_start:i_end], delays[i_start:i_end]],axis=None)
  return d_tmp
	
nof_files=file_idx_end-file_idx_start+1
s=(nof_files,2*(i_end-i_start))
xy_data=np.ones(s)*-1
p = Pool(16)

xy_list=p.map(get_data, range(nof_files))
for i in range(nof_files):
    xy_data[i,:]=xy_list[i]

np.save('./data_' + config + '_' + str(file_idx_start) + '_' + str(file_idx_end) + '.npy',xy_data)
p.close()
p.join()
