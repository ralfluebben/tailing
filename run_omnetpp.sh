# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

#!/bin/bash
#configs=( "mm1_25" "mm1_50" "mm1_75" "mw1_25" "mw1_50" "mw1_75" "ww1_25" "ww1_50" "ww1_75" ) 
configs=( "periodic_on_off" ) 
#array=( 0 100 )
#array=( 0 100000 200000 300000 400000)
#array=( 0 100000 200000)

for c in "${configs[@]}"
do
    for i in $(seq 0 10000 299999)
    do
        j=$(($i + 9999))
        #j=$(($i + 99))
        echo $i
        echo $j
        perl /usr/bin/parallel ./fifo -u Cmdenv -c ${c} -r {} ::: `seq -s ' ' $i $j`
        cd results
        parallel scavetool x ${c}-#{}.vec -o ${c}-#{}.csv ::: `seq -s ' ' $i $j`
        find ./ -name "${c}-*vec" -exec rm {} \;
        find ./ -name "${c}-*sca" -exec rm {} \;
        find ./ -name "${c}-*vci" -exec rm {} \;
	    python3 ~/git/dsl_prediction/train2000_from_csv_parallel_with_TA.py $i $j $c
        find ./ -name "${c}-*csv" -exec rm {} \;
        cd ..
    done
done
