# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

year=2020
month=11
days="01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 16 20 21 22 23 24 25 26 27 28 29 30 31"
for day in $days
do
    archiv_name="gs://archive-measurement-lab/ndt/pcap/$year/$month/$day/*pcap-mlab1-ham02*"
    gsutil cp -n $archiv_name ./
    filename="${year}${month}${day}*"
    find ./ -name  "$filename" | parallel -j12 tar xvzf {}
done

find ./ -size +10k -name "*.pcap.gz" | parallel -j12 bash ~/git/dsl_prediction/extract_timestamp_mlab.sh {}
