# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import numpy as np
data_tag = [ "on"  ]
load = [ "periodic_on_off"  ]
#data_mm1_25_0_99999.npy
# v = [ 0, 100000, 200000, 300000, 400000 ]
v = range(0,300000,10000)
inc = 9999
data = None
for t in data_tag:
    for l in load:
        data = None
        for i in range(0, len(v)):
            ve = v[i] + inc
            print(i)
            fn = 'data_' + t + "_" + l + "_" + str(v[i]) + "_" + str(ve) + ".npy"
            fn = 'data_'  + l + "_" + str(v[i]) + "_" + str(ve) + ".npy"
            if data is None:
                data = np.load(fn)
                data = np.float32(data)
                print(data.dtype)
                print(data.shape)
            else:
                tmp = np.load(fn)
                tmp = np.float32(tmp)
                d1 = data
                data = np.concatenate([d1, tmp], axis = 0)
                del tmp, d1
                print(data.dtype)
                print(data.shape)
        print(data.shape)
        fn = 'data_' + t + "_" + l + ".npy"
        fn = 'data_'  + l + ".npy"
        np.save(fn, data)
        del data