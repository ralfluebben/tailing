# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import numpy as np
import matplotlib.pyplot as plt
import sys
filename_pred = './preddata_input_mixed_model_%s.npy' % (sys.argv[1])
filename_test = './testdata_input_mixed_model_%s.npy' % (sys.argv[1])
x=np.load(filename_pred)
y=np.load(filename_test)

abs_mean_err =  np.sum(np.abs(x-y))/x.shape[0]
quantile_err = float(np.sum(y>=x))/float(len(y))
xl1=50000
xl2=xl1+250
plt.plot(x[xl1:xl2],label='prediction 95% qunatile')
plt.plot(y[xl1:xl2],label='test data')

textstr = "abs mean err=%lf , quantile predicted=%lf" % (abs_mean_err,quantile_err)
plt.text(0.05, 0.05, textstr, verticalalignment='top')
plt.legend(loc='upper left')
plt.show()
