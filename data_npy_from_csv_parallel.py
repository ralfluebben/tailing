# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
from multiprocessing import Pool

routing = pd.read_csv('./routing.csv.exp')
routing = pd.read_csv('./routing_0.csv')
vectors = routing[routing.type=='vector']
TAvec = vectors[vectors.name == 'TA:vector'].iloc[0]
TDvec = vectors[vectors.name == 'TD:vector'].iloc[0]
TAvecnp = np.fromstring(TAvec.vecvalue, dtype=float, sep=' ')
TDvecnp = np.fromstring(TDvec.vecvalue, dtype=float, sep=' ')

def get_data(i_start):
  global delays
  global interarrivaltimes
  global i_inc
  i_end=i_start+i_inc
  d_start=delays[i_start:i_end]
  iats=interarrivaltimes[i_start:i_end]
  d_end=delays[i_end+100]
  d_tmp=np.concatenate([d_start, iats, d_end],axis=None)
  return d_tmp
	
	

def generate_i_starts(ilen, i_inc ):
  i_starts=[]
  i=np.random.geometric(p=0.01)
  while i < (ilen-1-i_inc):
    i_starts.append(i)
    i_end=i+i_inc
    i=i_end+np.random.geometric(p=0.01)
  return i_starts
		

i_inc=50
first_run=True
i=np.random.geometric(p=0.01)
delays=TDvecnp-TAvecnp
interarrivaltimes=TAvecnp[1:len(TAvecnp)]-TAvecnp[0:len(TAvecnp)-1]

ilen=len(TAvecnp)-1-2*i_inc-100
i_starts = generate_i_starts(ilen, i_inc)
print i_starts

#i_starts=i_starts[0:1000]
s=(len(i_starts), 101)
xy_data=np.ones(s)*-1
p = Pool(32)
print delays.shape
print i_starts[-1]
xy_list=p.map(get_data, i_starts)
for i in range(0,len(i_starts)):
    xy_data[i,:]=xy_list[i]

#for i in i_starts:
#  d_tmp=get_data(delays, interarrivaltimes, i, i_inc)
#  if first_run==True:
#    xy_data=[d_tmp]
#    first_run=False
#  else:
#    xy_data=np.concatenate([xy_data,[d_tmp]],axis=0)

print xy_data.shape
data=xy_data
n = xy_data.shape[0]
p = xy_data.shape[1]

print xy_data[1,:]
print xy_data[999,:]

# Training and test data
train_start = 0
train_end = int(np.floor(0.8*n))
test_start = train_end + 1
test_end = n
data_train = data[np.arange(train_start, train_end), :]
data_test = data[np.arange(test_start, test_end), :]

data_test=np.save('./data_test_exp.nparr.npy',data_test)
data_train=np.save('./data_train_exp.nparr.npy',data_train)

quit()


