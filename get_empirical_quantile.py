# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import os
import tensorflow as tf
print(tf.__version__)

#os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

gpus = tf.config.list_physical_devices('GPU')
for g in gpus:
  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.experimental.set_memory_growth(g, True)
    #tf.config.experimental.set_virtual_device_configuration(
    #    gpus[0],
    #    [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=3500)])
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)

import gc
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
import argparse
from tensorboard.plugins.hparams import api as tf_hp
from tensorflow import keras

import sys
import math

import metrics
import windowgenerator

parser = argparse.ArgumentParser()

parser.add_argument("-s","--input_start", help="Packet to start observation", type=int, required=True)
parser.add_argument("-w","--input_width", help="Trainlength used for prediction", type=int, required=True)
parser.add_argument("-l","--label_width", help="Output prediction length", type=int, required=True)
parser.add_argument("-d","--data_tag", help="Input data", type=str, required=True)
args = parser.parse_args()

input_start = args.input_start
input_width = args.input_width
label_width = args.label_width
data_tag = args.data_tag

data_tags=None
begin_of_delays = 3000
use_testing_parameters = False
if data_tag == "mm1_75" or data_tag == "mw1_75" or data_tag == "ww1_75" or data_tag == "ww1_50" or data_tag == "ww1_25" or data_tag == "ARTA" or data_tag == "CHEP" or  data_tag == "Dist" or data_tag == "MAP": 
  data_filename = 'data/data_' + data_tag + '.npy'
  data = np.load(data_filename)
  data=data[0:300000,:]

data=np.reshape(data,(-1,begin_of_delays,2), order='F')
print(data.shape)
print(np.quantile(data[:,-1,1], 0.95))