# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Copyright 2023 Ralf Lübben

import os
import os.path
import numpy as np

data_aggregated = None
for dirpath, dirnames, filenames in os.walk("."):
    for filename in [f for f in filenames if f.endswith(".npy")]:
        fn = os.path.join(dirpath, filename)
        #print(fn)
        data_tmp = np.load(fn)
        #data_tmp = np.load("ndt-gsw5g_1596818457_000000000016AAF6.npy")
        delays = data_tmp[0:799,1]-data_tmp[0:799,3]
        iats = data_tmp[1:800,3]-data_tmp[0:799,3]
        #print(delays[0:50])
        data_tmp = np.reshape(np.concatenate([iats[:], delays[:]]),(-1,1))
        
        if data_aggregated is None:
            data_aggregated = data_tmp
        else:
            data_aggregated = np.concatenate([data_aggregated, data_tmp], axis=1)
            #print(data_aggregated[1590:1599,-1])
            #print(data_aggregated[790:800,-1])
            print(data_aggregated.shape)
np.save("data_mlab.npy", data_aggregated)